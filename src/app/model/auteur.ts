export class Auteur {
  id_auteur?: number;
  nom_auteur: string;
  prenom_auteur: string;

  constructor(values: object = {}) {
    Object.assign(this, values);
  }
}
