import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuteurComponent} from './page/auteur/auteur.component';


const routes: Routes = [
  {path: 'auteur/:id', component: AuteurComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
