import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Auteur} from '../model/auteur';

const API_URL = 'http://localhost:8001';

@Injectable({
  providedIn: 'root'
})
export class AuteurService {

  constructor(private http: HttpClient) { }

  getOne(id: number): Observable<Auteur> {
    // Création de l'entête de la requête à l'aide d'un objet HttpHeaders
    const headers = new HttpHeaders({Accept: 'application/json'});
    // Construction de l'url à qui envoyer la requête
    const url = `${API_URL}/?target=auteur&id=${id}`;
    // Envoie de la requête par la méthode GET
    return this.http.get<Auteur>(url, {headers});
  }

  getAll(): Observable<Auteur[]> {
    return null;
  }
}
