import { Component, OnInit } from '@angular/core';
import {Auteur} from '../../model/auteur';
import {AuteurService} from '../../service/auteur.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-auteur',
  templateUrl: './auteur.component.html',
  styleUrls: ['./auteur.component.css']
})
export class AuteurComponent implements OnInit {

  private auteur = new Auteur();
  private id: number;

  constructor(private service: AuteurService, private route: ActivatedRoute) { }

  ngOnInit() {
    // Récupération de l'id de l'auteur à afficher contenu dans l'url
    this.id = +this.route.snapshot.paramMap.get('id');
    // Utilisation du service pour récupérer l'auteur grâce à l'API REST (php)
    this.service.getOne(this.id).subscribe(result => this.auteur = result);
    // this.service.getOne(this.id).subscribe() -> récupère un résultat (ici je l'appelle result)
    // subscribe a besoin de savoir quoi faire avec result,  je lui dis que this.auteur = result
  }

}
